<?php

namespace Scito\Keycloak\Admin;

use Closure;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\HandlerStack;
use Scito\Keycloak\Admin\Contracts\GuzzleClientFactoryInterface;
use Scito\Keycloak\Admin\Guzzle\DefaultHeadersMiddleware;
use Scito\Keycloak\Admin\Guzzle\TokenMiddleware;
use Scito\Keycloak\Admin\Hydrator\Hydrator;
use Scito\Keycloak\Admin\Resources\ResourceFactory;
use Scito\Keycloak\Admin\Token\TokenManager;

class ClientBuilder
{
    private string $realm;

    private ?string $serverUrl = null;

    private ?string $clientId = null;

    private ?string $clientSecret = null;

    private ?string $token = null;

    private ?string $username = null;

    private ?string $password = null;

    private ?TokenManager $tokenManager = null;

    private mixed $guzzleClientFactory;

    public function __construct()
    {
        $this->realm = 'master';
        $this->guzzleClientFactory = function ($config) {
            return new GuzzleClient($config);
        };
    }

    /**
     * @param string $realm
     * @return ClientBuilder
     */
    public function withRealm(string $realm): self
    {
        $this->realm = $realm;
        return $this;
    }

    /**
     * @param string $url
     * @return ClientBuilder
     */
    public function withServerUrl(string $url): self
    {
        $this->serverUrl = $url;

        return $this;
    }

    /**
     * @param null|string $clientId
     * @return ClientBuilder
     */
    public function withClientId(?string $clientId): self
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * @param null|string $secret
     * @return ClientBuilder
     */
    public function withClientSecret(?string $secret): self
    {
        $this->clientSecret = $secret;

        return $this;
    }

    /**
     * @param string $username
     * @return ClientBuilder
     */
    public function withUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @param string $password
     * @return ClientBuilder
     */
    public function withPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @param null|string $token
     * @return ClientBuilder
     */
    public function withAuthToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function withTokenManager(?TokenManager $tokenManager): static
    {
        $this->tokenManager = $tokenManager;

        return $this;
    }

    public function build(): Client
    {
        if (null === ($tokenManager = $this->tokenManager)) {
            $tokenManager = $this->buildTokenManager($this->createGuzzleClient());
        }

        $tokenMiddleware = new TokenMiddleware($tokenManager, $this->realm);

        $stack = HandlerStack::create();

        $stack->push($tokenMiddleware);
        $stack->push(new DefaultHeadersMiddleware());

        $client = $this->createGuzzleClient([
            'handler' => $stack,
        ]);

        $factory = new ResourceFactory($client, new Hydrator());

        return new Client($factory, $this->realm, $this->clientId);
    }

    /**
     * @param callable $factory
     * @return $this
     */
    public function withGuzzleClientFactory(callable $factory): static
    {
        $this->guzzleClientFactory = $factory;

        return $this;
    }

    private function createGuzzleClient(array $baseConfig = []): GuzzleClient
    {
        $config = array_merge([
            'http_errors' => false,
            'base_uri' => $this->serverUrl,
            'verify' => true,
        ], $baseConfig);

        if ($this->guzzleClientFactory instanceof Closure) {
            return ($this->guzzleClientFactory)($config);
        }

        if ($this->guzzleClientFactory instanceof GuzzleClientFactoryInterface) {
            return $this->guzzleClientFactory->createGuzzleClient($config);
        }

        return new GuzzleClient($config);
    }

    private function buildTokenManager(ClientInterface $guzzle): TokenManager
    {
        return new TokenManager($this->username, $this->password, $this->clientId, $guzzle);
    }
}
