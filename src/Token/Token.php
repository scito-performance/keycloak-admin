<?php

namespace Scito\Keycloak\Admin\Token;

use DateTime;
use function date_create;

class Token
{
    public function __construct(private string $type, private string $token, private DateTime $expires)
    {
        $this->type = ucfirst($this->type);
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function __toString()
    {
        return $this->getContent();
    }

    public function getContent(): string
    {
        return $this->token;
    }

    public function isValid(): bool
    {
        return $this->expires < date_create();
    }
}
