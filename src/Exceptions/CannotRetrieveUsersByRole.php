<?php

namespace Scito\Keycloak\Admin\Exceptions;

use RuntimeException;

class CannotRetrieveUsersByRole extends RuntimeException
{

}
