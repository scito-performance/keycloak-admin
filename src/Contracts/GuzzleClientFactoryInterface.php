<?php

namespace Scito\Keycloak\Admin\Contracts;

use GuzzleHttp\ClientInterface;

interface GuzzleClientFactoryInterface
{
    public function createGuzzleClient(array $options): ClientInterface;
}
