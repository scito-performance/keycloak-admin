# Howto build the new container and push to gitlab

```
cd docker
docker login registry.gitlab.com
docker build -f app/Dockerfile -t keycloak-admin:latest ./app
docker tag keycloak-admin registry.gitlab.com/scito-performance/keycloak-admin
docker push registry.gitlab.com/scito-performance/keycloak-admin:latest
```
