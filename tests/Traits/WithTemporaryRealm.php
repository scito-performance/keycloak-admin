<?php

namespace Scito\Keycloak\Admin\Tests\Traits;

trait WithTemporaryRealm
{
    use WithFaker, WithTestClient;

    protected ?string $temporaryRealm;

    /**
     * @before
     */
    public function setupTemporaryRealmClass()
    {
        $this->temporaryRealm = $this->faker->userName();
        $this->client
            ->realms()
            ->create()
            ->name($this->temporaryRealm)
            ->enabled(true)
            ->save();
    }

    /**
     * @after
     */
    public function teardownTemporaryRealmClass()
    {
        $this->client
            ->realm($this->temporaryRealm)
            ->delete();
        $this->temporaryRealm = null;
    }
}
